//============================================================================
// Name        : RadTools.cpp
// Author      : Steffen Kremer
// Version     :
// Copyright   : Games-Table.Studio
// Description : RadTools Console
//============================================================================

#include "RadResourceLibrary.hpp"
#include <vector>

const int version = 1;

void printHead()
{
	std::cout << "Rad Tools v" << version << " (c)2017 by Games-Table.studio" << std::endl << std::endl;
}

void printHelp()
{
	std::cout << "RadTools [tool] [tool Arguments]" << std::endl << std::endl;
	std::cout << "Tool: help - show this text" << std::endl;
	std::cout << "RadTools help" << std::endl;
	std::cout << "RadTools \\h" << std::endl;
	std::cout << "RadTools \\?" << std::endl;
	std::cout << "Tool: resource - Pack resources with the RadResourceLibrary" << std::endl;
	std::cout << "RadTools resource [\\A] [[\\C] [\\F] [\\R]] [\\E] <filePath> <containerFile>" << std::endl;
	std::cout << "\\A - Add mode, folder or file" << std::endl;
	std::cout << "\\C - + Compress files" << std::endl;
	std::cout << "\\F - + Add mode folder" << std::endl;
	std::cout << "\\R - ++ Add folder recursive" << std::endl;
	std::cout << "\\E - Extract mode" << std::endl;
	std::cout << "path - File or folder path" << std::endl;
	std::cout << "path - Target container file" << std::endl;
}

int main(int argc, char* argv[])
{
	printHead();
	if (argc==1)
	{
		printHelp();
		return 0;
	}
	//Fetch arguments
	std::vector<std::string> argArray;
	for (int i=1;i<argc;i++)
	{
		//print arguments
		//std::cout << argv[i] << std::endl;
		argArray.push_back(argv[i]);
	}
	std::string mode = argArray.at(0);
	std::string exe = argArray[0];
	if (mode=="help"||mode=="\\h"||mode=="\\?")
	{
		printHelp();
		return 0;
	}
	else if (mode=="resource")
	{
		if (argArray.size()<2)
		{
			std::cout << "Too less args for this mode!" << std::endl;
			return 0;
		}
		rad::resourcelibrary::CContainer tmp;
		tmp.SetSTDOUT(true);
		if (argArray.at(1)=="\\A"||argArray.at(1)=="\\a")
		{
			bool cmp = false;
			bool fld = false;
			bool fldRcs = false;
			if (argArray.at(2)=="\\C"||argArray.at(2)=="\\c")
			{
				cmp = true;
			}
			if (argArray.at(2)=="\\F"||argArray.at(2)=="\\f"||argArray.at(3)=="\\F"||argArray.at(3)=="\\f")
			{
				fld = true;
				if (argArray.at(3)=="\\R"||argArray.at(3)=="\\r"||argArray.at(4)=="\\R"||argArray.at(4)=="\\r")
				{
					fldRcs = true;
				}
			}
			std::string file;
			std::string contFile;
			file = argArray.at(argArray.size()-1-1);
			contFile = argArray.at(argArray.size()-1);
			if (fld)
			{
				std::cout << "Adding folder..." << std::endl;
			}
			else
			{
				std::cout << "Adding file..." << std::endl;
			}
			std::cout << "File: " << file << std::endl << "Container:" << contFile << std::endl << std::endl;
			tmp.Open(contFile);
			if (fld)
			{
				tmp.AddFolder(file, cmp, fldRcs);
			}
			else
			{
				tmp.AddFile(file, cmp);
			}
			tmp.Close();
		}
		else if (argArray.at(1)=="\\E"||argArray.at(1)=="\\e")
		{
			std::cout << "Mode not finished yet." << std::endl;
		}
		else
		{
			std::cout << "No mode selected!" << std::endl;
		}
	}
	else
	{
		std::cout << "Error: Mode " << mode << " does not exist!" << std::endl << std::endl;
		printHelp();
		return 0;
	}
	return 0;
}
